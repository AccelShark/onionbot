#include <Wire.h>

byte stepper_orders[4] = {0, 0, 0, 0};
void setup() {
  // put your setup code here, to run once:
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(11, OUTPUT);
  digitalWrite(2, LOW);
  digitalWrite(4, LOW);
  analogWrite(3, 0);
  digitalWrite(11, 0);
  Wire.begin(0x69);                // join i2c bus with address #8
  Wire.onReceive(receiveEvent); // register event
}

void loop() {
  delay(100);
}

void receiveEvent(int howMany) {
  // put your main code here, to run repeatedly:
  if(howMany == 5) {
    Wire.read();
    for(byte i = 0; i < 4; i++) { stepper_orders[i] = Wire.read(); }
    if(stepper_orders[0] == 0x7F) {
      digitalWrite(2, LOW);
      analogWrite(3, stepper_orders[1]);
    } else if(stepper_orders[0] == 0x81) {
      digitalWrite(2, HIGH);
      analogWrite(3, stepper_orders[1]);
    } else {
      digitalWrite(2, LOW);
      analogWrite(3, 0);
    }
    if(stepper_orders[2] == 0x7F) {
      digitalWrite(4, LOW);
      analogWrite(11, stepper_orders[3]);
    } else if(stepper_orders[2] == 0x81) {
      digitalWrite(4, HIGH);
      analogWrite(11, stepper_orders[3]);
    } else {
      digitalWrite(4, LOW);
      analogWrite(11, 0);
    }
  }
}
