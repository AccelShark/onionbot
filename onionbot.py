#!/usr/bin/env python3
import asyncio
from OmegaExpansion import onionI2C
import logging

FORMAT = '%(asctime)s %(message)s'
logging.basicConfig(format=FORMAT, level=logging.INFO)
i2c = onionI2C.OnionI2C(0)

class OnionI2C:
    handler = None

    def __init__(self):
        self.logger = logging.getLogger("OnionI2C")
        self.logger.info("init onion i2c")

    async def read(self):
        pass

    async def write(self, dev_addr: int, addr: int, value: [chr]):
        i2c.writeBytes(dev_addr, addr, value)


class OnionContext:
    ticks = 0

    def __init__(self):
        self.logger = logging.getLogger("OnionContext")
        self.i2c = OnionI2C()

    async def loop(self):
        await self.i2c.write(0x69, 0x10, [0x81, 0xFF, 0x7F, 0xFF])
        await asyncio.sleep(1)
        await self.i2c.write(0x69, 0x10, [0x7F, 0xFF, 0x81, 0xFF])
        await asyncio.sleep(1)
        self.ticks += 1


if __name__ == '__main__':
    # Run loop
    onion = OnionContext()
    loop = asyncio.get_event_loop()
    while True:
        try:
            loop.run_until_complete(onion.loop())
        except KeyboardInterrupt as e:
            # brake and break
            loop.run_until_complete(onion.i2c.write(0x69, 0x10, [0x80, 0x00, 0x80, 0x00]))
            break
    loop.close()
    exit(0)
